#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>    //exit() 

int main(int argc, char * argv[])
{
	struct stat infobuf;   
	char *filename;         

	if (argc != 2) { 
        printf("Please input filename\n"); 
        exit(-1); 
    	} 
 	filename = argv[1];
	if ( stat(filename, &infobuf) == -1 )
		perror(filename);
	else
		printf(" The size of %s is %d\n",filename, infobuf.st_size );
}
