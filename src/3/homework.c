int g(int x)
{
  return x + 52;
}

int f(int x)
{
  return g(x);
}

int main(void)
{
  return f(0) + 8;
}
