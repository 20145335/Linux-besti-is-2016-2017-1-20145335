g:
	pushl	%ebp		;将%ebp入栈，作为帧指针
	movl	%esp, %ebp	
	movl	8(%ebp), %eax
	addl	$52, %eax
	popl	%ebp
	ret			;将%ebp出栈
f:
	pushl	%ebp		;将%ebp入栈，作为帧指针
	movl	%esp, %ebp
	pushl	8(%ebp)
	call	g		;调用g函数
	addl	$4, %esp
	leave			;为返回准备栈
	ret			;从过程调用中返回
main:
	pushl	%ebp		;将%ebp入栈，作为帧指针
	movl	%esp, %ebp
	pushl	$0
	call	f		;调用f函数
	addl	$4, %esp
	addl	$8, %eax
	leave			;为返回准备栈
	ret			;从过程调用中返回
